package org.virthorld;

import org.virthorld.elements.VEWAbstractIntegerResource;
import org.virthorld.elements.VEWResource;
import org.virthorld.exceptions.VEWResourceException;
import org.virthorld.exceptions.VEWResourceLevelOutOfBoundException;

public class StrengthResource extends VEWAbstractIntegerResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Integer RESOURCE_MIN_VALUE = 0;
	public static Integer RESOURCE_MAX_VALUE = 10000;
	
	public StrengthResource() {
		super("Strength");
	}
	
	public StrengthResource(String type) {
		super(type);
	}
	
	public StrengthResource(String type, Integer value) {
		super(type, value);
	}
	
	@Override
	public Integer getMaxAllowedValue() {
		return StrengthResource.RESOURCE_MAX_VALUE;
	}

	@Override
	public Integer getMinAllowedValue() {
		return StrengthResource.RESOURCE_MIN_VALUE;
	}	
	
	@Override
	public void increaseResourceValue(Object deltaValue) throws VEWResourceException, VEWResourceLevelOutOfBoundException {
		if( (StrengthResource.RESOURCE_MAX_VALUE-(Integer)deltaValue) < (Integer)this.getResourceValue()){
			throw new VEWResourceLevelOutOfBoundException("Final resource value is above the upper bound.");
		} else {
			this.resourceValue+=(Integer)deltaValue;
		}
	}
	
	@Override
	public void decreaseResourceValue(Object deltaValue) throws VEWResourceException, VEWResourceLevelOutOfBoundException {
		if( ( StrengthResource.RESOURCE_MIN_VALUE+(Integer)deltaValue) > (Integer)this.getResourceValue()){
			throw new VEWResourceLevelOutOfBoundException("Final resource value is below the lower bound.");
		}
		this.resourceValue-=(Integer)deltaValue;
	}

	@Override
	public VEWResource divideResource(Object value) throws VEWResourceException, VEWResourceLevelOutOfBoundException {
		if(!(value instanceof Integer)){
			throw new VEWResourceException("Incompatible type of resource value.");
		} else {
			Integer typedValue = (Integer)value;
			this.decreaseResourceValue(typedValue);
			VEWResource r = new StrengthResource(this.getResourceType(),typedValue);
			return r;
		}
	}

}
