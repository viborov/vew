package org.virthorld.elements;


import java.util.Set;

import org.virthorld.exceptions.VEWResourceException;

public interface VEWResourceFactory {
	public VEWResource getResource(String resourceType) throws VEWResourceException;
	public Set<String> getAvailableResourceTypes();

}
