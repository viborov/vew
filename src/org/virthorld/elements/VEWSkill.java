package org.virthorld.elements;

import org.virthorld.exceptions.VEWIncompatibleSkillException;

public interface VEWSkill {
	
	public String getSkillType();
	public void bootstrapInitiateSkill(VEWObject o) throws VEWIncompatibleSkillException;
	public VEWSkilled getSkillBearer();

}
