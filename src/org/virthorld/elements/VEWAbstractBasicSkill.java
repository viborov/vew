package org.virthorld.elements;

import org.virthorld.exceptions.VEWIncompatibleSkillException;

public abstract class VEWAbstractBasicSkill implements VEWSkill {

	public static String SKILLTYPE;
	private VEWSkilled skillBearer;

	public VEWAbstractBasicSkill() {
		this.setSkillType();
	}
	
	@Override
	public VEWSkilled getSkillBearer() {
		return this.skillBearer;
	}
	
	@Override
	public String getSkillType() {
		return VEWAbstractBasicSkill.SKILLTYPE;
	}
	
	protected void setSkillType(){
		VEWAbstractBasicSkill.SKILLTYPE=this.getClass().getCanonicalName();
	}


	@Override
	public void bootstrapInitiateSkill(VEWObject o) throws VEWIncompatibleSkillException {
		if(!((o instanceof VEWActor)&&(o instanceof VEWSkilled))){
			throw new VEWIncompatibleSkillException("Incomaptible skill.");
		} else {
			this.skillBearer=(VEWSkilled) o;
		}
	}



}
