package org.virthorld.elements;

public interface VEWLeveledSkill {
	public Integer getSkillLevel();
	public boolean isEqualLevel(VEWLeveledSkill s);
	public boolean isHigherLevel(VEWLeveledSkill s);
	public boolean getLowerLevel(VEWLeveledSkill s);

}
