package org.virthorld.elements;

import org.virthorld.exceptions.VEWConcurrencyException;
import org.virthorld.exceptions.VEWFailureException;
import org.virthorld.exceptions.VEWIncompatibleSkillException;

public interface VEWSkilled {
	public void acquireSkill(VEWSkill s) throws VEWConcurrencyException, VEWFailureException, VEWIncompatibleSkillException;
	public void looseSkill(VEWSkill s);
	public void trainSkill(VEWSkill s);
	public void useSkill(VEWSkill s) throws VEWConcurrencyException, VEWFailureException;

}
