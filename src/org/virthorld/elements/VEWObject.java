package org.virthorld.elements;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import org.virthold.system.VEWSystem;
import org.virthorld.exceptions.VEWConcurrencyException;
import org.virthorld.exceptions.VEWFailureException;
import org.virthorld.exceptions.VEWResourceException;
import org.virthorld.exceptions.VEWResourceLevelOutOfBoundException;

public abstract class VEWObject implements Serializable, VEWResourceful {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name = null;
	private Map<String, VEWResource> resources = new LinkedHashMap<>();
	private VEWSystem universe;
	
	public VEWObject(String objName, VEWSystem sys){
		this.setName(objName);
		this.universe = sys;
	}
	

	public abstract void applyAction(VEWAction a)  throws VEWConcurrencyException, VEWFailureException;
	
	public void consumeResource(VEWResource r) throws VEWResourceException, VEWResourceLevelOutOfBoundException{
		if(this.getResources().keySet().contains(r.getResourceType())){
			//System.err.println(this.getName()+" consolidating {"+r.getResourceType()+"/"+r.getResourceValue()+"}");
			this.getResources().get(r.getResourceType()).consolidateResource(r);
		} else{
			//System.err.println(this.getName()+" getting new {"+r.getResourceType()+"/"+r.getResourceValue()+"}");
			this.resources.put(r.getResourceType(), r);
		}
	}
	
	@Override
	public Map<String, VEWResource> getResources() {
		return resources;
	}

	@Override
	public VEWResource getResource(String resourceType) {
		return this.getResources().get(resourceType);
	}

	
	public void pickUpResource(VEWResource r){
		
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @param name the name to set
	 */
	private void setName(String name) {
		this.name = name;
	}

	public VEWSystem getVEWSystem(){
		return this.universe;
	}

}
