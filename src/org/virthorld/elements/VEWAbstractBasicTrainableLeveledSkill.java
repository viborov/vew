package org.virthorld.elements;

public abstract class VEWAbstractBasicTrainableLeveledSkill extends VEWAbstractBasicSkill implements VEWTrainableSkill, VEWLeveledSkill{


	private Integer skillLevel=0;

	public VEWAbstractBasicTrainableLeveledSkill() {
		super();
	}
	

	/*
	 * 
	 * Leveled Skill 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	
	
	@Override
	public Integer getSkillLevel() {
		return this.skillLevel;
	}

	@Override
	public boolean isEqualLevel(VEWLeveledSkill s) {
		return this.getSkillLevel()==s.getSkillLevel();
	}

	@Override
	public boolean isHigherLevel(VEWLeveledSkill s) {
		return this.getSkillLevel()>s.getSkillLevel();
	}

	@Override
	public boolean getLowerLevel(VEWLeveledSkill s) {
		return this.getSkillLevel()<s.getSkillLevel();
	}
	/*
	 * 
	 * Trainable Skill 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	@Override
	public void trainSkill(VEWSkill s) {
		this.levelUp();
		
	}

	@Override
	public void levelUp() {
		this.skillLevel=this.getSkillLevel()+1;
	}

	@Override
	public void levelDown() {
		this.skillLevel=this.getSkillLevel()-1;
	}
	
	
	
	/*
	 * 
	 *  Skill 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */


}
