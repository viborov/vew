package org.virthorld.elements;

import java.util.Map;

import org.virthorld.exceptions.VEWResourceException;
import org.virthorld.exceptions.VEWResourceLevelOutOfBoundException;

public interface VEWResourceful {

	public void consumeResource(VEWResource r) throws VEWResourceException, VEWResourceLevelOutOfBoundException;
	public VEWResource getResource(String resourceType);
	public Map<String, VEWResource> getResources();
}
