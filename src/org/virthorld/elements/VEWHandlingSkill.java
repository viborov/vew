package org.virthorld.elements;

import org.virthorld.exceptions.VEWFailureException;
import org.virthorld.exceptions.VEWHandlingException;

public interface VEWHandlingSkill {
	boolean canHandle(VEWAction a);
	void handle(VEWAction a) throws VEWFailureException, VEWHandlingException;
	boolean canHandle(VEWResourceDamageEffect e);
	void handle(VEWResourceDamageEffect e) throws VEWFailureException, VEWHandlingException;
	boolean canHandle(VEWObject o);
	void handle(VEWObject o) throws VEWFailureException, VEWHandlingException;
	boolean canHandle(VEWActor a);
	void handle(VEWActor a)  throws VEWFailureException, VEWHandlingException;
	boolean canHandle(VEWSkill s);
	void handle(VEWSkill s) throws VEWFailureException, VEWHandlingException;
}
