package org.virthorld.elements;

public interface VEWResourceDamageEffect {
	public VEWResource getResourceDamage();
	public void setResourceDamage(VEWResource r);
	public Object getEffectValue();
	public void apply(VEWObject o);
	public String getType();

}
