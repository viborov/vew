package org.virthorld.elements;

import java.util.Set;
import org.virthorld.exceptions.*;

public interface VEWSkillFactory {
	public VEWSkill getSkill(String skillType) throws VEWSkillException;
	public Set<String> getAvailableSkillTypes();
}
