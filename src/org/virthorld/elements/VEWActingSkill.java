package org.virthorld.elements;

import org.virthorld.exceptions.VEWActionException;
import org.virthorld.exceptions.VEWConcurrencyException;
import org.virthorld.exceptions.VEWFailureException;
import org.virthorld.exceptions.VEWResourceException;
import org.virthorld.exceptions.VEWResourceLevelOutOfBoundException;

public interface VEWActingSkill {
	boolean canActUppon(VEWAction a);
	VEWActionResult actUppon(VEWAction a) throws VEWFailureException, VEWActionException, VEWConcurrencyException, VEWResourceException;
	boolean canActUppon(VEWResourceDamageEffect e);
	VEWActionResult actUppon(VEWResourceDamageEffect e) throws VEWFailureException, VEWActionException, VEWConcurrencyException, VEWResourceException;
	boolean canActUppon(VEWObject o);
	VEWActionResult actUppon(VEWObject o) throws VEWFailureException, VEWActionException, VEWConcurrencyException, VEWResourceException;
	boolean canActUppon(VEWActor a);
	VEWActionResult actUppon(VEWActor a) throws VEWFailureException, VEWActionException, VEWConcurrencyException, VEWResourceException, VEWResourceLevelOutOfBoundException;
	boolean canActUppon(VEWSkill s);
	VEWActionResult actUppon(VEWSkill s) throws VEWFailureException, VEWActionException, VEWConcurrencyException, VEWResourceException;
}
