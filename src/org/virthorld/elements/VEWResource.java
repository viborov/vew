package org.virthorld.elements;

import org.virthorld.exceptions.VEWResourceException;
import org.virthorld.exceptions.VEWResourceLevelOutOfBoundException;

public interface VEWResource {
	
	public boolean isSameType(VEWResource r);
	public String getResourceType();
	public Object getResourceValue();
	public void increaseResourceValue(Object deltaValue) throws VEWResourceException, VEWResourceLevelOutOfBoundException;
	public void decreaseResourceValue(Object deltaValue) throws VEWResourceException, VEWResourceLevelOutOfBoundException;
	public void consolidateResource(VEWResource r) throws VEWResourceException, VEWResourceLevelOutOfBoundException;
	public VEWResource divideResource(Object value) throws VEWResourceException, VEWResourceLevelOutOfBoundException;
}
