package org.virthorld.elements;

public interface VEWAction {

	public String getName();

	public VEWResourceDamageEffect getActionEffect();

}
