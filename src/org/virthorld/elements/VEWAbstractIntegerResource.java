package org.virthorld.elements;


import java.io.Serializable;
import org.virthorld.exceptions.VEWResourceException;
import org.virthorld.exceptions.VEWResourceLevelOutOfBoundException;


/* 
 * 
 * 
 * This class represents basic resource with Integer-based value.
 * It is intended to be used in most of the scenarios with only 
 * minor adjustments.
 * 
 * 20160916
 */

public abstract class VEWAbstractIntegerResource implements Serializable, VEWResource {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static Integer RESOURCE_MIN_VALUE = Integer.MIN_VALUE;
	public static Integer RESOURCE_MAX_VALUE = Integer.MAX_VALUE;
	
	protected String resourceType;
	protected Integer resourceValue;
	
	public VEWAbstractIntegerResource(){
		this("AbstractInteger");
	}
	
	public VEWAbstractIntegerResource(String type){
		this(type,0);
	}

	public VEWAbstractIntegerResource(String type, Integer value){
		this.resourceType=type;
		this.resourceValue=value;
	}
	
	public abstract Integer getMaxAllowedValue();

	public abstract Integer getMinAllowedValue();

	@Override
	public boolean isSameType(VEWResource r) {
		return this.getResourceType().equals(r.getResourceType());
	}

	@Override
	public String getResourceType() {
		return this.resourceType;
	}

	@Override
	public Object getResourceValue() {
		return this.resourceValue;
	}
	
	@Override
	public void increaseResourceValue(Object deltaValue) throws VEWResourceLevelOutOfBoundException, VEWResourceException {
		if( (VEWAbstractIntegerResource.RESOURCE_MAX_VALUE-(Integer)deltaValue) < (Integer)this.getResourceValue()){
			throw new VEWResourceLevelOutOfBoundException("Final resource value is above the upper bound.");
		} else {
			this.resourceValue+=(Integer)deltaValue;
		}
	}
	
	@Override
	public void decreaseResourceValue(Object deltaValue) throws VEWResourceLevelOutOfBoundException, VEWResourceException {
		if((VEWAbstractIntegerResource.RESOURCE_MIN_VALUE+(Integer)deltaValue)>(Integer)this.getResourceValue()){
			throw new VEWResourceException("Final resource value is below the lower bound.");
		}
		if((this.resourceValue-=(Integer)deltaValue)<VEWAbstractIntegerResource.RESOURCE_MIN_VALUE){
			this.resourceValue=VEWAbstractIntegerResource.RESOURCE_MIN_VALUE;
				
		} else{
			this.resourceValue-=(Integer)deltaValue;
		}
	}

	@Override
	public void consolidateResource(VEWResource r) throws VEWResourceException, VEWResourceLevelOutOfBoundException, VEWResourceException {
		if(this.isSameType(r)){
			this.increaseResourceValue((Integer)r.getResourceValue());
			((VEWResource)r).decreaseResourceValue((Integer)r.getResourceValue());
		} else{
			throw new VEWResourceException("Uncompatible resource type.");
		}
	}

	/* Below is a typical solution (not thread-safe however) for dividing the resource in two.
	 * To use it - replace [_VEWAbstractIntegerResource_] with the name of your class.
	 * 
			if(!(value instanceof Integer)){
				throw new VEWResourceException("Incompatible type of Resource value.");
			} else {
				Integer typedValue = (Integer)value;
				this.decreaseResourceValue(typedValue);
				VEWResource r = new [_VEWAbstractIntegerResource_](this.getResourceType(),typedValue);
				return r;
			}
	*/
	@Override
	public abstract VEWResource divideResource(Object value) throws VEWResourceException, VEWResourceLevelOutOfBoundException;
}
