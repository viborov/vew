package org.virthorld.elements;

import org.virthold.system.VEWSystem;
import org.virthorld.exceptions.VEWConcurrencyException;
import org.virthorld.exceptions.VEWFailureException;
import org.virthorld.exceptions.VEWResourceException;
import org.virthorld.exceptions.VEWResourceLevelOutOfBoundException;

public interface VEWActor {
	
	public void recieveAction (VEWAction action, VEWSkilled hitActionInflicter) throws VEWConcurrencyException, VEWFailureException, VEWResourceException, VEWResourceLevelOutOfBoundException;
	public String getName();


}
