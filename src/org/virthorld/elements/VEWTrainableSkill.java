package org.virthorld.elements;

public interface VEWTrainableSkill {
	public void trainSkill(VEWSkill s);
	public void levelUp();
	public void levelDown();
}
