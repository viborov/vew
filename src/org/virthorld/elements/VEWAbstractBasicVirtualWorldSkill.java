package org.virthorld.elements;

import org.virthorld.exceptions.VEWActionException;
import org.virthorld.exceptions.VEWConcurrencyException;
import org.virthorld.exceptions.VEWFailureException;
import org.virthorld.exceptions.VEWHandlingException;
import org.virthorld.exceptions.VEWResourceException;
import org.virthorld.exceptions.VEWResourceLevelOutOfBoundException;

public abstract class VEWAbstractBasicVirtualWorldSkill extends VEWAbstractBasicTrainableLeveledSkill
		implements VEWHandlingSkill, VEWActingSkill {

	
	public VEWAbstractBasicVirtualWorldSkill() {
		super();
	}
	
	@Override
	public boolean canHandle(VEWAction a) {
		return false;
	}

	@Override
	public void handle(VEWAction a) throws VEWHandlingException {
		if(!this.canHandle(a)){
			throw new VEWHandlingException("Skill {"+this.getSkillType()+"/"+this.getSkillLevel()+"} cannot be used against action {"+a.getName()+"}.");
		}
	}

	@Override
	public boolean canHandle(VEWResourceDamageEffect e) {
		return false;
	}

	@Override
	public void handle(VEWResourceDamageEffect e) throws VEWHandlingException {
		if(!this.canHandle(e)){
			throw new VEWHandlingException("Skill {"+this.getSkillType()+"/"+this.getSkillLevel()+"} cannot be used against action {"+e.getType()+"}.");
		}
	}

	@Override
	public boolean canHandle(VEWObject o) {
		return false;
	}

	@Override
	public void handle(VEWObject o) throws VEWHandlingException {
		if(!this.canHandle(o)){
			throw new VEWHandlingException("Skill {"+this.getSkillType()+"/"+this.getSkillLevel()+"} cannot be used against action {"+o.getName()+"}.");
		}
	}

	@Override
	public boolean canHandle(VEWActor a) {
		return false;
	}

	@Override
	public void handle(VEWActor a) throws VEWHandlingException {
		if(!this.canHandle(a)){
			throw new VEWHandlingException("Skill {"+this.getSkillType()+"/"+this.getSkillLevel()+"} cannot be used against action {"+a.getName()+"}.");
		}
	}

	@Override
	public boolean canHandle(VEWSkill s) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void handle(VEWSkill s) throws VEWHandlingException {
		if(!this.canHandle(s)){
			throw new VEWHandlingException("Skill {"+this.getSkillType()+"/"+this.getSkillLevel()+"} cannot be used against action {"+s.getSkillType()+"}.");
		}
	}

	@Override
	public boolean canActUppon(VEWAction a) {
		return false;
	}

	@Override
	public VEWActionResult actUppon(VEWAction a) throws VEWFailureException, VEWActionException, VEWConcurrencyException, VEWResourceException {
		if(!this.canActUppon(a)){
			throw new VEWActionException("Skill {"+this.getSkillType()+"/"+this.getSkillLevel()+"} cannot be used against action {"+a.getName()+"}.");
		}
		return new VEWActionResult(true);
	}

	@Override
	public boolean canActUppon(VEWResourceDamageEffect e) {
		return false;
	}

	@Override
	public VEWActionResult actUppon(VEWResourceDamageEffect e) throws VEWFailureException, VEWActionException, VEWConcurrencyException, VEWResourceException {
		if(!this.canActUppon(e)){
			throw new VEWActionException("Skill {"+this.getSkillType()+"/"+this.getSkillLevel()+"} cannot be used against effect {"+e.getType()+"}.");
		}
		return new VEWActionResult(true);
	}

	@Override
	public boolean canActUppon(VEWObject o) {
		return false;
	}

	@Override
	public VEWActionResult actUppon(VEWObject o) throws VEWFailureException, VEWActionException, VEWConcurrencyException, VEWResourceException{
		if(!this.canActUppon(o)){
			throw new VEWActionException("Skill {"+this.getSkillType()+"/"+this.getSkillLevel()+"} cannot be used against object {"+o.getName()+"}.");
		}
		return new VEWActionResult(true);
	}

	@Override
	public boolean canActUppon(VEWActor a) {
		return false;
	}

	@Override
	public VEWActionResult actUppon(VEWActor a) throws VEWFailureException, VEWActionException, VEWConcurrencyException, VEWResourceException, VEWResourceLevelOutOfBoundException {
		if(!this.canActUppon(a)){
			throw new VEWActionException("Skill {"+this.getSkillType()+"/"+this.getSkillLevel()+"} cannot be used against actor {"+a.getName()+"}.");
		}
		return new VEWActionResult(true);
	}

	@Override
	public boolean canActUppon(VEWSkill s) {
		return false;
	}

	@Override
	public VEWActionResult actUppon(VEWSkill s) throws VEWFailureException, VEWActionException, VEWConcurrencyException, VEWResourceException {
		if(!this.canActUppon(s)){
			throw new VEWActionException("Skill {"+this.getSkillType()+"/"+this.getSkillLevel()+"} cannot be used against skill {"+s.getSkillType()+"}.");
		}
		return new VEWActionResult(true);
	}
	
}
