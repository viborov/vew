package org.virthorld.elements;

import java.io.Serializable;

public class VEWActionResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Boolean actionSuccessful=null; 
	private VEWResourceDamageEffect actionEffect=null;
	private VEWAction backAction=null;
	
	public VEWActionResult(Boolean wasSuccessful){
		this.setActionSuccessful(wasSuccessful);
	}

	public Boolean getActionSuccessful() {
		return actionSuccessful;
	}

	public void setActionSuccessful(Boolean actionSuccessful) {
		this.actionSuccessful = actionSuccessful;
	}

	public VEWResourceDamageEffect getActionEffect() {
		return actionEffect;
	}

	public void setActionEffect(VEWResourceDamageEffect actionEffect) {
		this.actionEffect = actionEffect;
	}

	public VEWAction getBackAction() {
		return backAction;
	}

	public void setBackAction(VEWAction backAction) {
		this.backAction = backAction;
	}
	
	
	

}
