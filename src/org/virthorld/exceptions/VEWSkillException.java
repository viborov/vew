package org.virthorld.exceptions;

public class VEWSkillException extends VEWException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VEWSkillException(String s, Throwable t) {
		super(s,t);
	}

	public VEWSkillException(Throwable t) {
		super(t);
	}

	public VEWSkillException(String s) {
		super(s);
	}
}
