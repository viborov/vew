package org.virthorld.exceptions;

public class VEWResourceLevelOutOfBoundException extends VEWException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VEWResourceLevelOutOfBoundException(String s, Throwable t) {
		super(s,t);
	}

	public VEWResourceLevelOutOfBoundException(Throwable t) {
		super(t);
	}

	public VEWResourceLevelOutOfBoundException(String s) {
		super(s);
	}
}
