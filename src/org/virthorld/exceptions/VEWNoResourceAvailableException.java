package org.virthorld.exceptions;

public class VEWNoResourceAvailableException extends VEWException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VEWNoResourceAvailableException(String s, Throwable t) {
		// TODO Auto-generated constructor stub
		super(s,t);
	}

	public VEWNoResourceAvailableException(Throwable t) {
		// TODO Auto-generated constructor stub
		super(t);
	}

	public VEWNoResourceAvailableException(String s) {
		// TODO Auto-generated constructor stub
		super(s);
	}
}
