package org.virthorld.exceptions;

public class VEWFailureException extends VEWException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VEWFailureException(String s, Throwable t) {
		// TODO Auto-generated constructor stub
		super(s,t);
	}

	public VEWFailureException(Throwable t) {
		// TODO Auto-generated constructor stub
		super(t);
	}

	public VEWFailureException(String s) {
		// TODO Auto-generated constructor stub
		super(s);
	}
}
