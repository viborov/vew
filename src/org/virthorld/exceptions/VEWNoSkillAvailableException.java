package org.virthorld.exceptions;

public class VEWNoSkillAvailableException extends VEWException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VEWNoSkillAvailableException(String s, Throwable t) {
		// TODO Auto-generated constructor stub
		super(s,t);
	}

	public VEWNoSkillAvailableException(Throwable t) {
		// TODO Auto-generated constructor stub
		super(t);
	}

	public VEWNoSkillAvailableException(String s) {
		// TODO Auto-generated constructor stub
		super(s);
	}
}
