package org.virthorld.exceptions;

public class VEWHandlingException extends VEWException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VEWHandlingException(String s, Throwable t) {
		super(s,t);
	}

	public VEWHandlingException(Throwable t) {
		super(t);
	}

	public VEWHandlingException(String s) {
		super(s);
	}
}
