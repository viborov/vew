package org.virthorld.exceptions;

public class VEWConcurrencyException extends VEWException {

	public VEWConcurrencyException(String s, Throwable t) {
		// TODO Auto-generated constructor stub
		super(s,t);
	}

	public VEWConcurrencyException(Throwable t) {
		// TODO Auto-generated constructor stub
		super(t);
	}

	public VEWConcurrencyException(String s) {
		// TODO Auto-generated constructor stub
		super(s);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
