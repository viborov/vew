package org.virthorld.exceptions;

public class VEWResourceException extends VEWException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VEWResourceException(String s, Throwable t) {
		
		super(s,t);
	}

	public VEWResourceException(Throwable t) {
		
		super(t);
	}

	public VEWResourceException(String s) {
		
		super(s);
	}
}
