package org.virthorld.exceptions;

public class VEWActionException extends VEWException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VEWActionException(String s, Throwable t) {
		// TODO Auto-generated constructor stub
		super(s,t);
	}

	public VEWActionException(Throwable t) {
		// TODO Auto-generated constructor stub
		super(t);
	}

	public VEWActionException(String s) {
		// TODO Auto-generated constructor stub
		super(s);
	}
}
