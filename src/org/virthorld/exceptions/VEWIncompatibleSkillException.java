package org.virthorld.exceptions;

public class VEWIncompatibleSkillException extends VEWException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VEWIncompatibleSkillException(String s, Throwable t) {
		// TODO Auto-generated constructor stub
		super(s,t);
	}

	public VEWIncompatibleSkillException(Throwable t) {
		// TODO Auto-generated constructor stub
		super(t);
	}

	public VEWIncompatibleSkillException(String s) {
		// TODO Auto-generated constructor stub
		super(s);
	}
}
