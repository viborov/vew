package org.virthorld;

import org.virthorld.elements.VEWAbstractIntegerResource;
import org.virthorld.elements.VEWResource;
import org.virthorld.exceptions.VEWResourceException;

public class MagicManaResource extends VEWAbstractIntegerResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Integer RESOURCE_MIN_VALUE = 0;
	public static Integer RESOURCE_MAX_VALUE = 1000;
	
	public MagicManaResource() {
		super("Mana");
	}
	
	public MagicManaResource(String type) {
		super(type);
	}
	
	public MagicManaResource(String type, Integer value) {
		super(type, value);
	}
	
	@Override
	public Integer getMaxAllowedValue() {
		return MagicManaResource.RESOURCE_MAX_VALUE;
	}

	@Override
	public Integer getMinAllowedValue() {
		return MagicManaResource.RESOURCE_MIN_VALUE;
	}	

	@Override
	public void increaseResourceValue(Object deltaValue) throws VEWResourceException {
		if( (MagicManaResource.RESOURCE_MAX_VALUE-(Integer)deltaValue) < (Integer)this.getResourceValue()){
			throw new VEWResourceException("Final resource value is above the upper bound.");
		} else {
			this.resourceValue+=(Integer)deltaValue;
		}
	}
	
	@Override
	public void decreaseResourceValue(Object deltaValue) throws VEWResourceException {
		if( ( MagicManaResource.RESOURCE_MIN_VALUE+(Integer)deltaValue) > (Integer)this.getResourceValue()){
			throw new VEWResourceException("Final resource value is below the lower bound.");
		}
		this.resourceValue-=(Integer)deltaValue;
	}
	
	@Override
	public VEWResource divideResource(Object value) throws VEWResourceException {
		if(!(value instanceof Integer)){
			throw new VEWResourceException("Incompatible type of resource value.");
		} else {
			Integer typedValue = (Integer)value;
			this.decreaseResourceValue(typedValue);
			VEWResource r = new MagicManaResource(this.getResourceType(),typedValue);
			return r;
		}
	}

}
