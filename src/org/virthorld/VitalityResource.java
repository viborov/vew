package org.virthorld;

import org.virthorld.elements.VEWAbstractIntegerResource;
import org.virthorld.elements.VEWResource;
import org.virthorld.exceptions.VEWResourceException;
import org.virthorld.exceptions.VEWResourceLevelOutOfBoundException;

public class VitalityResource extends VEWAbstractIntegerResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Integer RESOURCE_MIN_VALUE = 0;
	public static Integer RESOURCE_MAX_VALUE = 1000;

	public VitalityResource(){
		this("Vitality");
	}
	
	public VitalityResource(String type) {
		super(type);
	}

	public VitalityResource(String type, Integer value) {
		super(type, value);
	}
	
	@Override
	public Integer getMaxAllowedValue() {
		return VitalityResource.RESOURCE_MAX_VALUE;
	}

	@Override
	public Integer getMinAllowedValue() {
		return VitalityResource.RESOURCE_MIN_VALUE;
	}

	@Override
	public void increaseResourceValue(Object deltaValue) throws VEWResourceException {
		if( (VitalityResource.RESOURCE_MAX_VALUE-(Integer)deltaValue) < (Integer)this.getResourceValue()){
			throw new VEWResourceException("Final resource value is above the upper bound.");
		} else {
			this.resourceValue+=(Integer)deltaValue;
		}
	}
	
	@Override
	public void decreaseResourceValue(Object deltaValue) throws VEWResourceException, VEWResourceLevelOutOfBoundException {
		if( ( VitalityResource.RESOURCE_MAX_VALUE+(Integer)this.getResourceValue()<(Integer)deltaValue)){
			throw new VEWResourceLevelOutOfBoundException("Final resource value is below the lower bound.");
		}
		if((this.resourceValue-=(Integer)deltaValue)<VitalityResource.RESOURCE_MIN_VALUE){
			this.resourceValue=VitalityResource.RESOURCE_MIN_VALUE;
		} else{
			this.resourceValue-=(Integer)deltaValue;
		}
	}

	@Override
	public VEWResource divideResource(Object value) throws VEWResourceException, VEWResourceLevelOutOfBoundException {
		if(!(value instanceof Integer)){
			throw new VEWResourceException("Incompatible type of Resource value.");
		} else {
			Integer typedValue = (Integer)value;
			this.decreaseResourceValue(typedValue);
			VEWResource r = new VitalityResource(this.getResourceType(),typedValue);
			return r;
		}
	}

}
