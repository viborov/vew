package org.virthorld;

import org.virthorld.elements.VEWActor;
import org.virthorld.elements.VEWObject;
import org.virthorld.elements.VEWSkill;
import org.virthorld.elements.VEWSkilled;
import org.virthorld.exceptions.VEWIncompatibleSkillException;

public class VEWBasicSkill2 implements VEWSkill {

	private String skillType;
	private VEWSkilled skillBearer;

	public VEWBasicSkill2() {
		this("VEWBasicSkill2");
	}
	
	public VEWBasicSkill2(String type) {
		this.skillType=type;
	}
	
	@Override
	public VEWSkilled getSkillBearer() {
		return this.skillBearer;
	}
	@Override
	public String getSkillType() {
		return this.skillType;
	}

	@Override
	public void bootstrapInitiateSkill(VEWObject o) throws VEWIncompatibleSkillException {
		if(!((o instanceof VEWActor)&&(o instanceof VEWSkilled)&&(o instanceof VEWSkill))){
			throw new VEWIncompatibleSkillException("Incomaptible skill.");
		} else {
			this.skillBearer=(VEWSkilled) o;
		}
	}



}
