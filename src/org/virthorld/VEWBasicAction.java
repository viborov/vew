package org.virthorld;

import org.virthorld.elements.VEWAction;
import org.virthorld.elements.VEWResourceDamageEffect;

public class VEWBasicAction implements VEWAction {
	public static String ACTIONTYPE;
	private VEWResourceDamageEffect actionEffect;
	
	public VEWBasicAction (VEWResourceDamageEffect e){
		this.setName(this.getClass().getCanonicalName());
		this.setActionEffect(e);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return VEWBasicAction.ACTIONTYPE;
	}

	/**
	 * @param name the name to set
	 */
	private void setName(String type) {
		VEWBasicAction.ACTIONTYPE = type;
	}

	/**
	 * @return the actionEffect
	 */
	public VEWResourceDamageEffect getActionEffect() {
		return this.actionEffect;
	}

	/**
	 * @param actionEffect the actionEffect to set
	 */
	public void setActionEffect(VEWResourceDamageEffect actionEffect) {
		this.actionEffect = actionEffect;
	}
	

}
