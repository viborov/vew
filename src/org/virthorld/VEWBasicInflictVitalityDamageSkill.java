package org.virthorld;

import org.virthold.system.BasicResourceFactory;
import org.virthorld.elements.VEWAbstractBasicVirtualWorldSkill;
import org.virthorld.elements.VEWAction;
import org.virthorld.elements.VEWActionResult;
import org.virthorld.elements.VEWActor;
import org.virthorld.elements.VEWObject;
import org.virthorld.elements.VEWResourceDamageEffect;
import org.virthorld.elements.VEWResource;
import org.virthorld.elements.VEWResourceFactory;
import org.virthorld.elements.VEWResourceful;
import org.virthorld.elements.VEWSkilled;
import org.virthorld.exceptions.VEWActionException;
import org.virthorld.exceptions.VEWConcurrencyException;
import org.virthorld.exceptions.VEWFailureException;
import org.virthorld.exceptions.VEWResourceException;
import org.virthorld.exceptions.VEWResourceLevelOutOfBoundException;


public class VEWBasicInflictVitalityDamageSkill extends VEWAbstractBasicVirtualWorldSkill {
	
	
	
	public VEWBasicInflictVitalityDamageSkill() {
		super();
	}
	
	
	@Override
	public boolean canActUppon(VEWActor a) {
		if(a instanceof VEWResourceful){
			return ((VEWResourceful) a).getResource("Vitality")!=null;
		} else {
			return false;
		}
	}

	@Override
	public VEWActionResult actUppon(VEWActor a) throws VEWFailureException, VEWActionException, VEWConcurrencyException, VEWResourceException, VEWResourceLevelOutOfBoundException {
		if(!this.canActUppon(a)){
			throw new VEWActionException("Skill {"+this.getSkillType()+"/"+this.getSkillLevel()+"} cannot be used against actor {"+a.getName()+"}.");
		} else {
			Integer hitEffectLevel = 0;
			VEWSkilled hitActionInflicter = this.getSkillBearer();
			if(hitActionInflicter instanceof VEWResourceful){
				StrengthResource inflictActionPowerDeterminatingResource = (StrengthResource) ((VEWResourceful) hitActionInflicter).getResource("Strength");
				if(inflictActionPowerDeterminatingResource!=null){
					hitEffectLevel = (Integer) inflictActionPowerDeterminatingResource.getResourceValue();
					hitEffectLevel=(int)Math.round(Math.log10((float)hitEffectLevel));
					VEWResourceFactory rf = ((VEWObject)this.getSkillBearer()).getVEWSystem().getResourceFactoty();
					VEWResource damage = rf.getResource("Vitality");
					damage.decreaseResourceValue(hitEffectLevel);
					VEWResourceDamageEffect hitEffect = new VEWBasicResourceDamageEffect(damage);
					VEWAction hitAction = new VEWBasicAction(hitEffect);
					a.recieveAction(hitAction, hitActionInflicter);
				} else {
					
				}
			} else {

			}
			
			
			
			return new VEWActionResult(true);
		}
	}


}
