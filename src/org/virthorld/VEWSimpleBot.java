package org.virthorld;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.virthold.system.VEWSystem;
import org.virthorld.elements.VEWAction;
import org.virthorld.elements.VEWActor;
import org.virthorld.elements.VEWAlive;
import org.virthorld.elements.VEWObject;
import org.virthorld.elements.VEWResource;
import org.virthorld.elements.VEWResourceDamageEffect;
import org.virthorld.elements.VEWSkill;
import org.virthorld.elements.VEWSkilled;
import org.virthorld.elements.VEWTrainableSkill;
import org.virthorld.exceptions.VEWActionException;
import org.virthorld.exceptions.VEWConcurrencyException;
import org.virthorld.exceptions.VEWFailureException;
import org.virthorld.exceptions.VEWIncompatibleSkillException;
import org.virthorld.exceptions.VEWNoResourceAvailableException;
import org.virthorld.exceptions.VEWNoSkillAvailableException;
import org.virthorld.exceptions.VEWResourceException;
import org.virthorld.exceptions.VEWResourceLevelOutOfBoundException;
import org.virthorld.exceptions.VEWSkillException;

public class VEWSimpleBot extends VEWObject implements VEWActor, VEWAlive, VEWSkilled {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<String, VEWSkill> skills = new LinkedHashMap<String, VEWSkill>();

	private final Lock lockRecieveAction = new ReentrantLock();
	private final Lock lockInflictAction = new ReentrantLock();
	private final Lock lockProcessRecievedAction = new ReentrantLock();
	private final Lock lockSkillsAccess = new ReentrantLock();

	public static Long _MAXWAITLOCKTIME_MS_ = 10L;

	private Long attemptedInterractionsCount = 0L;

	public VEWSimpleBot(String n, VEWSystem sys) {
		super(n, sys);
		
	}

	private void inflictVEWAction(VEWAction action, VEWActor actor)
			throws VEWConcurrencyException, VEWFailureException, VEWResourceException, VEWResourceLevelOutOfBoundException {
		try {
			if (lockInflictAction.tryLock(VEWSimpleBot._MAXWAITLOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				
				actor.recieveAction(action, this);
				this.attemptedInterractionsCount++;
				action = null;

			} else {
				throw new VEWFailureException(this.getName() + " [" + Thread.currentThread().getName()
						+ "] reached timeout while waiting to begin interaction with " + actor.getName() + ").");
			}
		} catch (InterruptedException e) {
			
			throw new VEWConcurrencyException(this.getName() + 
					" [" + Thread.currentThread().getName() + "] "
					+ "interrupted while waiting to interact with " +
					this.getName() + ").", e);
		} finally {
			lockInflictAction.unlock();
		}
	}

	@Override
	public void recieveAction(VEWAction action, VEWSkilled actionInflictor)
			throws VEWConcurrencyException, VEWFailureException, VEWResourceException, VEWResourceLevelOutOfBoundException {
		
		try {
			if (lockRecieveAction.tryLock(VEWSimpleBot._MAXWAITLOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				try {
					String actionInflictorName = "=UNKNOWN=";
					if(actionInflictor instanceof VEWObject){
						actionInflictorName=((VEWObject)actionInflictor).getName();
					}
//					System.err.println(VEWSimpleBot.this.getName() + " recieved " + action.getName() + " from " + actionInflictorName);
					this.processRecievedAction(action, actionInflictor);
				} finally {
					lockRecieveAction.unlock();
				}
			} else {
				throw new VEWFailureException(this.getName() + " [" + Thread.currentThread().getName()
						+ "] reached timeout while waiting to begin interaction.");
			}
		} catch (InterruptedException e) {
			
			throw new VEWConcurrencyException(this.getName() + " [" + Thread.currentThread().getName()
					+ "] interrupted while waiting to begin interaction.", e);
		}
	}

	private void processRecievedAction(VEWAction action, VEWSkilled actionInflictor) throws VEWFailureException, VEWConcurrencyException, VEWResourceException, VEWResourceLevelOutOfBoundException {
		
		try {
			if (lockProcessRecievedAction.tryLock(VEWSimpleBot._MAXWAITLOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				
				VEWResourceDamageEffect effect = action.getActionEffect();

//				System.err.println(" @ @ @ @ @ @ @ > "+this.getName() + " has " + this.getResource("Vitality").getResourceValue());
//				System.err.println(" @ @ @ @ @ @ @ > "+this.getName() + " processing action:" + action.getName()+" bearing effect: "+effect.getClass().getCanonicalName()+"{"+effect.getResourceDamage().getResourceType()+"/"+effect.getResourceDamage().getResourceValue()+"}");
				this.consumeResource(effect.getResourceDamage()); 
//				System.err.println(" @ @ @ @ @ @ @ > "+this.getName() + " has " + this.getResource("Vitality").getResourceValue());

			} else {
				throw new VEWFailureException(this.getName() + " [" + Thread.currentThread().getName()
						+ "] reached timeout while waiting to start processing recieved action.");
			}
		} catch (InterruptedException e) {
			
			throw new VEWConcurrencyException(this.getName() + 
					" [" + Thread.currentThread().getName() + "] "
					+ "interrupted while waiting to start processing." , e);
		} finally {
			lockProcessRecievedAction.unlock();
		}
	}

	/*
	 * Skill handling section
	 * 
	 * 
	 * 	
	*/
	
	public Set<String> getSkillTypes(){
		return this.skills.keySet();
	}
	
	public Set<VEWSkill> getSkillSet(){
		return this.skills.values().stream().collect(Collectors.toSet());
	}
	
	public VEWSkill getSkill(String type){
		return this.skills.get(type);
	}
	
	
	@Override
	public void acquireSkill(VEWSkill s) throws VEWConcurrencyException, VEWFailureException, VEWIncompatibleSkillException {
		
		try {
			if (lockSkillsAccess.tryLock(VEWSimpleBot._MAXWAITLOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				try {
					//System.err.println(this.getName()+"has "+this.getSkills().size()+" and adding new skill {"+s.getSkillType()+"}");
					if(this.getSkillTypes().contains(s.getSkillType())){
						if(s instanceof VEWTrainableSkill){
							((VEWTrainableSkill)this.getSkill(s.getSkillType())).trainSkill(s);
						}
					}else{
						this.skills.put(s.getSkillType(), s);
						s.bootstrapInitiateSkill(this);
						//System.err.println(this.getName()+" done.");
					}

				} catch (VEWIncompatibleSkillException e){
					this.skills.remove(s.getSkillType());
					//System.err.println(this.getName()+"has "+this.getSkills().size()+" and cant add new skill{"+s.getSkillType()+"}");
					throw e;
				}finally {
					lockSkillsAccess.unlock();
				}
			} else {
				throw new VEWFailureException(this.getName() + " [" + Thread.currentThread().getName()
						+ "] reached timeout while waiting for adding a skill(" + s.getSkillType() + ").");
			}
		} catch (InterruptedException e) {
			
			throw new VEWConcurrencyException(this.getName() + " [" + Thread.currentThread().getName()
					+ "] interrupted while waiting for adding a skill(" + s.getSkillType() + ").", e);
		}

	}

	@Override
	public void useSkill(VEWSkill s) throws VEWConcurrencyException, VEWFailureException {
		// TODO Auto-generated method stub

	}



	@Override
	public void looseSkill(VEWSkill s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void trainSkill(VEWSkill s) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	/*
	 * 
	 * 
	 * 	
	*/

	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public void applyAction(VEWAction a) throws VEWConcurrencyException, VEWFailureException {
		// TODO Auto-generated method stub

	}
	

	@Override
	public void run() {
			try {
				
				this.runOneCycle();
				
			} catch (VEWConcurrencyException | VEWFailureException | VEWResourceException | VEWSkillException | VEWIncompatibleSkillException e) {
				e.printStackTrace();
			} catch (VEWResourceLevelOutOfBoundException | VEWActionException e) {

			} finally {
				try {
					VEWSystem.getInstance().queueForExecution(this);
				} catch (VEWConcurrencyException | VEWFailureException e) {
					e.printStackTrace();
				}
			}
	}

	private void runOneCycle() throws VEWConcurrencyException, VEWFailureException, VEWResourceException, VEWSkillException, VEWIncompatibleSkillException, VEWActionException, VEWResourceLevelOutOfBoundException {
//		System.err.println("= = = > "+this.getName() + " Cycle START.");
		this.applyAllSkills();
		this.pickUpResource();
		this.pickUpSkill();
		
//		System.err.println("= = = > "+this.getName() + " Cycle FINISH.");
		/* getting the existing actor to attempt to interact */
/*
		VEWActor interactionPartner = VEWSystem.getInstance().getRandomActor();

		if (interactionPartner.equals(this)) {
			//System.err.println(this.getName() + " skipping interaction with itself.");
		} else {
			//Create Action and inflict it on the chosen Actor.
			VEWAction action = new VEWBasicAction("::ACTION::" + this.attemptedInterractionsCount,(VEWEffect) new VEWBasicEffect(this.attemptedInterractionsCount));
			this.inflictVEWAction(action, interactionPartner);
		}
*/
	}
	
	private void applyAllSkills() throws VEWResourceException, VEWFailureException, VEWConcurrencyException, VEWActionException, VEWResourceLevelOutOfBoundException {
		for (VEWSkill s : this.getSkillSet()) {
			if(s instanceof VEWBasicInflictVitalityDamageSkill){
				
				VEWBasicInflictVitalityDamageSkill hitSkill = (VEWBasicInflictVitalityDamageSkill)s;
				VEWActor interactionPartner;

					interactionPartner = VEWSystem.getInstance().getRandomActor();
					if (interactionPartner.equals(this)) {
						//System.err.println("= = = > "+this.getName() + " skipping interaction with itself.");
					} else {
						//System.err.println("= = = > "+this.getName() + " applying Basic Vitality Damage Skill.");
						hitSkill.actUppon(interactionPartner);
					}



				
			}
		}
		
	}

	public void pickUpResource() throws VEWFailureException, VEWConcurrencyException, VEWResourceException, VEWResourceLevelOutOfBoundException{

		VEWResource r;
		try {
			r = VEWSystem.getInstance().getRandomResource();
			this.consumeResource(r);
			
			System.err.println(this.getName() + " has {" + r.getResourceType()+ "/" + this.getResources().get(r.getResourceType()).getResourceValue() +"}");
		} catch (VEWNoResourceAvailableException e) {
			//e.printStackTrace();
		}
	}


	public void pickUpSkill() throws VEWFailureException, VEWConcurrencyException, VEWSkillException, VEWIncompatibleSkillException{

		VEWSkill s;
		try {
			s = VEWSystem.getInstance().getRandomSkill();
			this.acquireSkill(s);
			
			System.err.println(this.getName() + " has " + this.getSkillSet().size()+ " skills.");
		} catch (VEWNoSkillAvailableException e) {
			//e.printStackTrace();
		}
	}


	
}
