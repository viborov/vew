package org.virthorld;

import org.virthorld.elements.VEWObject;
import org.virthorld.elements.VEWResource;
import org.virthorld.elements.VEWResourceDamageEffect;

public class VEWBasicResourceDamageEffect implements VEWResourceDamageEffect {
	
	private VEWResource resourceDamage;
	
	public VEWBasicResourceDamageEffect (VEWResource damage){
		this.setResourceDamage(damage);

	}


	@Override
	public void apply(VEWObject o) {
		// TODO Auto-generated method stub
		return;
		
	}

	public VEWResource getResourceDamage() {
		return this.resourceDamage;
	}

	public void setResourceDamage(VEWResource r) {
		this.resourceDamage = r;
	}

	@Override
	public Object getEffectValue(){
		return this.getResourceDamage().getResourceValue();
	}
	
	@Override
	public String getType() {
		return "Damage";
	}

}
