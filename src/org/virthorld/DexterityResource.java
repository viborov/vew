package org.virthorld;

import org.virthorld.elements.VEWAbstractIntegerResource;
import org.virthorld.elements.VEWResource;
import org.virthorld.exceptions.VEWResourceException;

public class DexterityResource extends VEWAbstractIntegerResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Integer RESOURCE_MIN_VALUE = -10;
	public static Integer RESOURCE_MAX_VALUE = 1000;
	
	public DexterityResource() {
		super("Dexterity");
	}
	
	public DexterityResource(String type) {
		super(type);
	}
	
	public DexterityResource(String type, Integer value) {
		super(type, value);
	}
	
	@Override
	public Integer getMaxAllowedValue() {
		return DexterityResource.RESOURCE_MAX_VALUE;
	}

	@Override
	public Integer getMinAllowedValue() {
		return DexterityResource.RESOURCE_MIN_VALUE;
	}	

	@Override
	public void increaseResourceValue(Object deltaValue) throws VEWResourceException {
		if( (DexterityResource.RESOURCE_MAX_VALUE-(Integer)deltaValue) < (Integer)this.getResourceValue()){
			throw new VEWResourceException("Final resource value is above the upper bound.");
		} else {
			this.resourceValue+=(Integer)deltaValue;
		}
	}
	
	@Override
	public void decreaseResourceValue(Object deltaValue) throws VEWResourceException {
		if( ( DexterityResource.RESOURCE_MIN_VALUE+(Integer)deltaValue) > (Integer)this.getResourceValue()){
			throw new VEWResourceException("Final resource value is below the lower bound.");
		}
		this.resourceValue-=(Integer)deltaValue;
	}
	
	@Override
	public VEWResource divideResource(Object value) throws VEWResourceException {
		if(!(value instanceof Integer)){
			throw new VEWResourceException("Incompatible type of resource value.");
		} else {
			Integer typedValue = (Integer)value;
			this.decreaseResourceValue(typedValue);
			VEWResource r = new DexterityResource(this.getResourceType(),typedValue);
			return r;
		}
	}

}
