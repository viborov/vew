package org.virthold.system;

import org.virthorld.elements.VEWActor;
import org.virthorld.exceptions.VEWConcurrencyException;
import org.virthorld.exceptions.VEWFailureException;

public interface VEWActorExecutor {
	
	public void queueForExecution(VEWActor a) throws VEWConcurrencyException, VEWFailureException;

}
