package org.virthold.system;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.virthorld.elements.VEWSkill;
import org.virthorld.elements.VEWSkillFactory;
import org.virthorld.exceptions.VEWSkillException;

public class BasicSkillFactory implements Serializable,VEWSkillFactory {

	private static final long serialVersionUID = 1L;
	
	//Holds resourceID as a key and ClassName as a value.
	public static Map<String, String> _AVALABLE_SKILLS_ = new LinkedHashMap<String, String>();
	static{
		_AVALABLE_SKILLS_.put("VEWBasicInflictVitalityDamageSkill", "org.virthorld.VEWBasicInflictVitalityDamageSkill");
//		_AVALABLE_SKILLS_.put("Punch", "org.virthorld.PunchSkill");
//		_AVALABLE_SKILLS_.put("SpellCast", "org.virthorld.SpellCastSkill");
//		_AVALABLE_SKILLS_.put("Push", "org.virthorld.PushSkill");
//		_AVALABLE_SKILLS_.put("Touch", "org.virthorld.TouchSkill");
	}
	@Override
	public VEWSkill getSkill(String skillType) throws VEWSkillException {
		if(!this.getAvailableSkillTypes().contains(skillType)){
			throw new VEWSkillException("Unsopported skill type.");
		} 
		try {
			return  (VEWSkill) Class.forName(BasicSkillFactory._AVALABLE_SKILLS_.get(skillType)).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new VEWSkillException("Unable to create new resource:"+skillType,e);
		}
	}

	@Override
	public Set<String> getAvailableSkillTypes() {
		return BasicSkillFactory._AVALABLE_SKILLS_.keySet();
	}

}
