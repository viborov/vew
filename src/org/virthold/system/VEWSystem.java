package org.virthold.system;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.virthorld.*;
import org.virthorld.VEWSimpleBot;
import org.virthorld.elements.VEWActor;
import org.virthorld.elements.VEWObject;
import org.virthorld.elements.VEWResource;
import org.virthorld.elements.VEWResourceFactory;
import org.virthorld.elements.VEWSkill;
import org.virthorld.exceptions.*;

public class VEWSystem implements Runnable, VEWActorExecutor {

	public static final Integer _MAX_THREADPOOL_SIZE = 1;
	public static final Integer _MAX_SIMPLEBOTS_COUNT_ = 2;
	public static final Long _MAX_WAIT_RESOURCES_LOCKTIME_MS_ = 2L;
	public static final Long _MAX_WAIT_SKILLS_LOCKTIME_MS_ = 2L;
	


	public static final Double _MAX_SIMPLE_RESOURSE_VALUE_ = 200.0;
	public static final Integer _MAX_VITALITY_RESOURSE_VALUE_ = VitalityResource.RESOURCE_MAX_VALUE/100;
	public static final Integer _MAX_MANA_RESOURSE_VALUE_ = MagicManaResource.RESOURCE_MAX_VALUE/100;
	public static final Integer _MAX_STRENGTH_RESOURSE_VALUE_ = StrengthResource.RESOURCE_MAX_VALUE/100;
	public static final Integer _MAX_DEXTERITY_RESOURSE_VALUE_ = DexterityResource.RESOURCE_MAX_VALUE/100;

	public static Long _MIN_ACTOR_SLEEPTIME_ = 300L;
	public static Long _MAX_ACTOR_SLEEPTIME_ = 500L;

	public static Long _MIN_SYSTEM_SLEEPTIME_ = 200L;
	public static Long _MAX_SYSTEM_SLEEPTIME_ = 300L;

	/*
	 * Objects for synchronization locks
	 */

	private final Lock lockActorsAccess = new ReentrantLock();
	private final Lock lockActorsAddRemove = new ReentrantLock();
	private final Lock lockResourcesAccess = new ReentrantLock();
	private final Lock lockResourcesAddRemove = new ReentrantLock();
	private final Lock lockSkillsAccess = new ReentrantLock();
	private final Lock lockSkillsAddRemove = new ReentrantLock();
	
	
	

	private static VEWSystem selfInstance = null;
	private static String configFileFullName = "C:\\Users\\michaelv\\workspace\\VirtualEarthWorld\\VEWConf\\Available VEWResources.json";

	private VEWResourceFactory resourceFactory;

	private Set<VEWObject> existingEntites = new HashSet<VEWObject>();
	private Set<VEWActor> existingActors = new HashSet<VEWActor>();
	private Set<VEWResource> resourcePool = new HashSet<VEWResource>();
	private Set<VEWSkill> skillPool = new HashSet<VEWSkill>();
	
	private ScheduledExecutorService scheduler; 

	private VEWSystem() throws VEWFailureException {
		System.out.println("Hello, I'm Virthold - Virtual Earth World !");
		this.scheduler = Executors.newScheduledThreadPool(VEWSystem._MAX_THREADPOOL_SIZE);
		
		this.resourceFactory = new BasicResourceFactory(VEWSystem.configFileFullName);
		this.initilize();
	}

	public static VEWSystem getInstance() throws VEWFailureException {
		if(VEWSystem.selfInstance==null){
			try {
				VEWSystem.selfInstance = new VEWSystem();
			} catch (VEWFailureException e) {
				throw new VEWFailureException("Cant initialize system.",e);
			}
		}
		return selfInstance;
	}

	/**
	 * @return the resourcePool
	 */
	public Set<VEWResource> getResources() {
		return resourcePool;
	}

	public Set<VEWSkill> getSkills() {
		return skillPool;
	}
	
	public VEWResourceFactory getResourceFactoty(){
		return this.resourceFactory;
	}

	/**
	 * @param resourcePool
	 *            the resourcePool to set
	 * @throws VEWFailureException 
	 * @throws VEWConcurrencyException 
	 */
	public void checkResource(VEWResource r) throws VEWFailureException, VEWConcurrencyException {
		try {
			if (this.lockResourcesAddRemove.tryLock(VEWSystem._MAX_WAIT_RESOURCES_LOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				try{
					this.resourcePool.add(r);
				} finally {
					lockResourcesAddRemove.unlock();
				}
			} else {
				throw new VEWFailureException("Timeout while waiting to check a resource.");
			}
		} catch (InterruptedException e) {
			throw new VEWConcurrencyException("Interrupted while waiting to obtain a resource.", e);
		}

		
	}
	
	public void uncheckResource(VEWResource r)  throws VEWFailureException, VEWConcurrencyException {
		try {
			if (this.lockResourcesAddRemove.tryLock(VEWSystem._MAX_WAIT_RESOURCES_LOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				try{
					this.resourcePool.remove(r);
				} finally {
					lockResourcesAddRemove.unlock();
				}
			} else {
				throw new VEWFailureException("Timeout while waiting to uncheck a resource.");
			}
		} catch (InterruptedException e) {
			throw new VEWConcurrencyException("Interrupted while waiting to obtain a resource.", e);
		}
	}

	
	
	
	
	public void checkSkill(VEWSkill s) throws VEWFailureException, VEWConcurrencyException {
		try {
			if (this.lockSkillsAddRemove.tryLock(VEWSystem._MAX_WAIT_SKILLS_LOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				try{
					this.skillPool.add(s);
				} finally {
					lockSkillsAddRemove.unlock();
				}
			} else {
				throw new VEWFailureException("Timeout while waiting to check a resource.");
			}
		} catch (InterruptedException e) {
			throw new VEWConcurrencyException("Interrupted while waiting to check a resource.", e);
		}

		
	}
	
	public void uncheckSkill(VEWSkill s)  throws VEWFailureException, VEWConcurrencyException {
		try {
			if (this.lockSkillsAddRemove.tryLock(VEWSystem._MAX_WAIT_RESOURCES_LOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				try{
					this.skillPool.remove(s);
				} finally {
					lockSkillsAddRemove.unlock();
				}
			} else {
				throw new VEWFailureException("Timeout while waiting to uncheck a resource.");
			}
		} catch (InterruptedException e) {
			throw new VEWConcurrencyException("Interrupted while waiting to uncheck a resource.", e);
		}
	}

	
	
	
	
	
	/**
	 * @return the existingEntites
	 */
	public Set<VEWObject> getEntites() {
		return existingEntites;
	}

	/**
	 * @param existingEntites
	 *            the existingEntites to set
	 */
	public void addEntity(VEWObject e) {
		this.existingEntites.add(e);
	}

	public void addActor(VEWActor a) throws VEWConcurrencyException, VEWFailureException {
		try {
			if (this.lockActorsAddRemove.tryLock(VEWSystem._MAX_WAIT_RESOURCES_LOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				try{
					
					this.addEntity((VEWObject) a);
					this.existingActors.add(a);
				} finally {
					lockActorsAddRemove.unlock();
				}
				
			} else {
				throw new VEWFailureException("Gave up waiting to get an Actor.");
			}
		} catch (InterruptedException e) {
			throw new VEWConcurrencyException("Interrupted while waiting to get an Actor.", e);
		}
	}

	/**
	 * @return the existingActors
	 */
	public Set<VEWActor> getExistingActors() {
		return existingActors;
	}

	public VEWActor getRandomActor() throws VEWFailureException, VEWConcurrencyException {
		try {
			if (this.lockActorsAccess.tryLock(VEWSystem._MAX_WAIT_RESOURCES_LOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				try{
					Integer retIdx = (int) (Math.random() * (this.getExistingActors().size() - 1));
					return (VEWActor) this.getExistingActors().toArray()[retIdx];
				} finally {
					lockActorsAccess.unlock();
				}
				
			} else {
				throw new VEWFailureException("Gave up waiting to get an Actor.");
			}
		} catch (InterruptedException e) {
			throw new VEWConcurrencyException("Interrupted while waiting to get an Actor.", e);
		}
	}
	
	
	public VEWResource getRandomResource() throws VEWNoResourceAvailableException, VEWFailureException, VEWConcurrencyException {
		try {
			if (this.lockResourcesAccess.tryLock(VEWSystem._MAX_WAIT_RESOURCES_LOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				try {
					Integer countAvailableResources = this.getResources().size();
//					System.err.println("Its " + countAvailableResources + " resources avaliable.");
					if (countAvailableResources == 0) {
//						lockResourcesAccess.unlock();
//						System.err.println("= = = = > Lock released. Throwing out.");
						throw new VEWNoResourceAvailableException("No resrouces available.");
					} else {
						Integer retIdx = (int) (Math.random() * (countAvailableResources - 1));
						VEWResource r = (VEWResource) this.getResources().toArray()[retIdx];
						this.uncheckResource(r);
						return r;
					}
				} finally {
					lockResourcesAccess.unlock();
				}
			} else {
				throw new VEWFailureException("Timeout waiting to obtain a resource.");
			}
		} catch (InterruptedException e) {
			throw new VEWConcurrencyException("Interrupted while waiting to obtain a resource.", e);
		}
	}

	public VEWSkill getRandomSkill() throws VEWNoSkillAvailableException, VEWFailureException, VEWConcurrencyException {
		try {
			if (this.lockSkillsAccess.tryLock(VEWSystem._MAX_WAIT_SKILLS_LOCKTIME_MS_, TimeUnit.MILLISECONDS)) {
				try {
					Integer countAvailableSkills = this.getSkills().size();
					if (countAvailableSkills == 0) {
						throw new VEWNoSkillAvailableException("No skills available.");
					} else {
						Integer retIdx = (int) (Math.random() * (countAvailableSkills - 1));
						VEWSkill s = (VEWSkill) this.getSkills().toArray()[retIdx];
						this.uncheckSkill(s);
						return s;
					}
				} finally {
					lockSkillsAccess.unlock();
				}
			} else {
				throw new VEWFailureException("Timeout waiting to obtain a skill.");
			}
		} catch (InterruptedException e) {
			throw new VEWConcurrencyException("Interrupted while waiting to obtain a skill.", e);
		}
	}

	

	private void initilize() {
		for (int i = 0; i < VEWSystem._MAX_SIMPLEBOTS_COUNT_; i++) {
			VEWActor a = new VEWSimpleBot("SiBot#" + (i + 1), this);
			try {
				this.addActor(a);
				//System.err.println("Added " + a.getName());
				this.queueForExecution(a, 16L);
			} catch (VEWConcurrencyException | VEWFailureException e1) {
				e1.printStackTrace();
			}
			try {
				Thread.sleep((long) 4);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	private void growRandomResource() throws VEWResourceException, VEWFailureException, VEWConcurrencyException, VEWResourceLevelOutOfBoundException{
		Set<String> availableResourceTypes = this.getResourceFactoty().getAvailableResourceTypes();
		
		Integer availableResourceTypesCnt=availableResourceTypes.size();
		if(availableResourceTypesCnt>0){
			Integer resTypeIdx = Math.toIntExact(Math.round(((availableResourceTypesCnt-1)*Math.random())));
			String genResourceID=(String) availableResourceTypes.toArray()[resTypeIdx];
			VEWResource r = this.getResourceFactoty().getResource(genResourceID);
			Integer incDelta = 0;
			switch(genResourceID){
			case "Vitality":
				incDelta = Math.toIntExact(Math.round(VEWSystem._MAX_VITALITY_RESOURSE_VALUE_*Math.random()));
				r.increaseResourceValue(incDelta);
				break;

			case "Mana":
				incDelta = Math.toIntExact(Math.round(VEWSystem._MAX_MANA_RESOURSE_VALUE_*Math.random()));
				r.increaseResourceValue(incDelta);
				break;

			case "Strength":
				incDelta = Math.toIntExact(Math.round(VEWSystem._MAX_STRENGTH_RESOURSE_VALUE_*Math.random()));
				r.increaseResourceValue(incDelta);
				break;

			case "Dexterity":
				incDelta = Math.toIntExact(Math.round(VEWSystem._MAX_DEXTERITY_RESOURSE_VALUE_*Math.random()));
				r.increaseResourceValue(incDelta);
				break;

			default: 
				//throw new VEWFailureException("Unsupported resource type:"+genResourceID);
			}
			this.checkResource(r);
		}
	}

	
	private void growRandomSkill() throws VEWSkillException, VEWFailureException, VEWConcurrencyException {
		Set<String> availableSkillTypes = new BasicSkillFactory().getAvailableSkillTypes();
		
		Integer availableSkillTypesCnt=availableSkillTypes.size();
		if(availableSkillTypesCnt>0){
			Integer skillTypeIdx = Math.toIntExact(Math.round(((availableSkillTypesCnt-1)*Math.random())));
			String genSkillID=(String) availableSkillTypes.toArray()[skillTypeIdx];
			VEWSkill s = (new BasicSkillFactory()).getSkill(genSkillID);
			switch(genSkillID){
			case "VEWBasicInflictVitalityDamageSkill":
				break;

			case "VEWBasicSkill2":
				break;

			default: 
				throw new VEWFailureException("Unsupported skill type:"+genSkillID);
			}
			this.checkSkill(s);
		}
	}

	private void runCycle(){
		try {
			for(int i=0;i<1;i++){
				this.growRandomResource();
				this.growRandomSkill();
			}
		
		} catch (VEWFailureException | VEWSkillException | VEWConcurrencyException | VEWResourceException e) {
			e.printStackTrace();
		} catch (VEWResourceLevelOutOfBoundException e) {

		}
		


	}
	
	@Override
	public void run() {
		Boolean stayAlife = true;
		while (stayAlife) {
			
			this.runCycle();

			//System.err.println("\nSystem running. {EN:" + this.getEntites().size() + ";RS:"+this.getResources().size()+";SK:"+this.getSkills().size()+"}");

			// Go to sleep till next iteration
			Long sleepTimeSpan = VEWSystem._MIN_SYSTEM_SLEEPTIME_ + Math.round((VEWSystem._MAX_SYSTEM_SLEEPTIME_-VEWSystem._MIN_SYSTEM_SLEEPTIME_)*Math.random());
			try {
				//System.err.println("System going to sleep for " + sleepTimeSpan + " msec.");
				Thread.sleep(sleepTimeSpan);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.scheduler.shutdown();
	}

	@Override
	public void queueForExecution(VEWActor a) throws VEWConcurrencyException, VEWFailureException {
		Long scheduledDelayMsec = VEWSystem._MIN_ACTOR_SLEEPTIME_
				+ Math.round((VEWSystem._MAX_ACTOR_SLEEPTIME_ - VEWSystem._MIN_ACTOR_SLEEPTIME_) * Math.random());
		
		this.queueForExecution(a, scheduledDelayMsec);

	}

	public void queueForExecution(VEWActor a, Long msec) throws VEWConcurrencyException, VEWFailureException {
		try {
			this.scheduler.schedule((Runnable) a, msec, TimeUnit.MILLISECONDS);
		} catch (RejectedExecutionException | NullPointerException e) {
			throw new VEWFailureException("Failure scheduling.", e);
		}
	}

	
}
