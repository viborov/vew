package org.virthold.system;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.virthorld.elements.VEWResource;
import org.virthorld.elements.VEWResourceFactory;
import org.virthorld.exceptions.VEWFailureException;
import org.virthorld.exceptions.VEWResourceException;




public class BasicResourceFactory implements Serializable, VEWResourceFactory{

	private static final long serialVersionUID = 1L;
	private String confFileName;
	
	
	public BasicResourceFactory (String configFile) throws VEWFailureException{
		this.setConfFileName(configFile);
		this.loadConfiguration();
	}
	
	//Holds resourceID as a key and ClassName as a value.
	private Map<String, String> availableResources = new LinkedHashMap<String, String>();




	public String getConfFileName() {
		return confFileName;
	}



	public void setConfFileName(String confFileName) {
		this.confFileName = confFileName;
	}
	
	public  void loadConfiguration() throws VEWFailureException{
		try {
			String jsonFL =this.getConfFileName();
//			System.err.println("Loading:" + jsonFL);
			JSONObject rootObject = new JSONObject(new String(Files.readAllBytes(Paths.get(jsonFL))));
			JSONArray resAll = rootObject.getJSONArray("Available VEWResources");
			for (int i = 0; i < resAll.length(); i++) {
				JSONObject resConfig = resAll.getJSONObject(i);
				String configResType = resConfig.getString("Resource Type");
				String configResClass = resConfig.getString("Resource ClassName");
				this.availableResources.put(configResType, configResClass);
			}

		} catch (JSONException | IOException e) {
			throw new VEWFailureException("Failed to load all configuration. Loaded "+this.availableResources.size()+" elements.", e);
		}
		
	}
	
	
	public VEWResource getResource(String resourceType) throws VEWResourceException{
		if(!this.getAvailableResourceTypes().contains(resourceType)){
			throw new VEWResourceException("Unsopported resource type.");
		} 
		try {
			return  (VEWResource) Class.forName(this.availableResources.get(resourceType)).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new VEWResourceException("Unable to create new resource:"+resourceType,e);
		}
		
	}



	@Override
	public Set<String> getAvailableResourceTypes() {
		return this.availableResources.keySet();
	}


}
